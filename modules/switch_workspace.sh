#! /bin/bash

case $1 in
   [1-9O])
      swaymsg "workspace $1"
      ;;
   up)
      swaymsg "workspace prev"
      ;;
   down)
      swaymsg "workspace next"
      ;;
esac

pkill -SIGRTMIN+11 waybar
