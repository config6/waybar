#!/bin/bash

tmpfile=~/.config/waybar/modules/spotify.tmp
curr_player=spotify
max_len=30

for player in $(playerctl -l); do
	if [[ $(playerctl  metadata --player=${player} --format "{{lc(status)}}") == "playing" ]]; then
		curr_player=$player
		break
	fi
done

status=$(playerctl metadata --player=${curr_player} --format '{{lc(status)}}')
if [[ $curr_player == "spotify" ]]; then
	icon=""
fi

info_artist=$(playerctl metadata --player=${curr_player} --format '{{artist}}')
info_title=$(playerctl metadata --player=${curr_player} --format '{{title}}')

if [ -z $info_title ]; then
   info_url=$(playerctl metadata xesam:url --player=${curr_player})
   # update info only on url change
   if [[ "`head -n 1 $tmpfile`" != "$info_url" ]]; then
      # remember url in first line
      echo $info_url > $tmpfile
      # store track data in second line
      webinfo=`curl $info_url | grep Spotify.Entity`

      info_title=${webinfo##*name\":\"}
      info_title=${info_title%%\"*}
      echo $info_title >> $tmpfile
      info_artist=${webinfo#*name*name*name\":\"}
      info_artist=${info_artist%%\"*}
      echo $info_artist >> $tmpfile
   fi
   info_title=`sed -n '2p' $tmpfile`
   info_artist=`sed -n '3p' $tmpfile`
fi

if [[ ${#info_artist} -gt $max_len ]]; then
   info_artist=$(echo $info_artist | cut -c1-$((max_len-1)))"…"
fi
if [[ ${#info_title} -gt $max_len ]]; then
   info_title=$(echo $info_title | cut -c1-$((max_len-1)))"…"
fi

info=$info_title" - "$info_artist
if [[ $status == "playing" ]]; then
	text=" "$info
elif [[ $status == "paused" ]]; then
	text=" "$info
elif [[ $status == "stopped" ]]; then
	text=""
fi

class=$(swaymsg -pt get_workspaces | grep Workspace\ O | sed 's/.*(\([^]]*\)).*/\1/g')

echo -e "{\"text\":\""$text"\", \"class\":\""$class"\"}"
