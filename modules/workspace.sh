#! /bin/bash

IFS=
tmpfile=~/.config/waybar/modules/workspace.tmp

if [ -z $1 ]; then
   workspace_info=`swaymsg -pt get_workspaces`
   if [[ "`cat $tmpfile`" != "$workspace_info" ]]; then
      echo $workspace_info > $tmpfile
      pkill -SIGRTMIN+10 waybar
   fi
else
   workspace=`grep -A 3 Workspace\ $1 $tmpfile`

   if [ -z $workspace ]; then
      class=invisible
      text=
   else
      if [[ `grep Workspace <<< $workspace` == *focused* ]]; then
         class=focused
      fi
      first=`grep Representation <<< $workspace | tr '[]()' ' ' | cut -f5 -d' '`
      text="$1 `~/.config/waybar/modules/icon_mapper.sh $first`"
   fi

   echo -e "{\"text\": \"$text\", \"class\": \"$class\"}"
fi
