#!/bin/env python

import argparse
from phue import Bridge


MINBRIGHTNESS = 1
MAXBRIGHTNESS = 254
MINTEMPERATURE = 153
MAXTEMPERATURE = 454

LAMPS = [1, 2]


parser = argparse.ArgumentParser(description="Control Philips Hue Lights")
parser.add_argument("--setb", help="set brightness")
parser.add_argument("--incb", help="increase brightness")
parser.add_argument("--decb", help="decrease brightness")
parser.add_argument("--getb", action="store_true", help="print brightness")
parser.add_argument("--sett", help="set temperature")
parser.add_argument("--inct", help="increase temperature")
parser.add_argument("--dect", help="decrease temperature")
parser.add_argument("--gett", action="store_true", help="print temperature")
parser.add_argument("-t", "--toggle", action="store_true", help="toggle lamp")

args = parser.parse_args()

b = Bridge("192.168.1.140")


def percental_value(value, min_value, max_value):
    value = int(min_value + value/100 * (max_value-min_value + 0.5))
    return value


def trim_value(value, min_value, max_value):
    value = max(min_value, min(max_value, value))
    return value


def parse_value(raw_value, min_value, max_value, old_value=0):
    if raw_value[-1] == '%':
        value = percental_value(float(raw_value[:-1]), min_value, max_value)
    else:
        value = int(raw_value)
    value = trim_value(old_value + value - min_value, min_value, max_value)
    return value


def get_percentage(value, min_value, max_value):
    percent = (value - min_value) / (max_value - min_value) * 100
    rounded_percent = int(percent + 0.5)
    return rounded_percent


if args.setb:
    value = parse_value(args.setb, MINBRIGHTNESS, MAXBRIGHTNESS)
    b.set_light(LAMPS, "bri", value)

if args.incb:
    old_value = b.get_light(1, "bri")
    value = parse_value(args.incb, MINBRIGHTNESS, MAXBRIGHTNESS, old_value)
    b.set_light(LAMPS, "bri", value)

if args.decb:
    old_value = b.get_light(1, "bri")
    value = parse_value('-' + args.decb, MINBRIGHTNESS, MAXBRIGHTNESS, old_value)
    b.set_light(LAMPS, "bri", value)

if args.getb:
    value = b.get_light(1, "bri")
    percent = get_percentage(value, MINBRIGHTNESS, MAXBRIGHTNESS)
    print(percent)

if args.sett:
    value = parse_value(args.sett, MINTEMPERATURE, MAXTEMPERATURE)
    b.set_light(LAMPS, "ct", value)

if args.inct:
    old_value = b.get_light(1, "ct")
    value = parse_value(args.inct, MINTEMPERATURE, MAXTEMPERATURE, old_value)
    b.set_light(LAMPS, "ct", value)

if args.dect:
    old_value = b.get_light(1, "ct")
    value = parse_value('-' + args.dect, MINTEMPERATURE, MAXTEMPERATURE, old_value)
    b.set_light(LAMPS, "ct", value)

if args.gett:
    value = b.get_light(1, "ct")
    percent = get_percentage(value, MINTEMPERATURE, MAXTEMPERATURE)
    print(percent)

if args.toggle:
    state = b.get_light(1, "on")
    b.set_light(LAMPS, "on", not state)


#value = calc_new_brightness(args.brightness)
#print("set to", value)
#b.set_light(1, "bri", value)
