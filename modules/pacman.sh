#!/bin/bash

updates=`checkupdates`
num_of_updates=`wc -l <<< $updates`
let num_of_updates--

if [ "$num_of_updates" -gt "0" ]; then
   class='pending'
else
   class=''
fi

# add string which will lead to newlines in tooltip
tooltip=`sed -e 's/$/\\\\n/' <<< $updates`
# remove original newlines which would lead to leading spaces
tooltip=`tr -d '\n' <<< $tooltip`
# remove trailing newline-string
tooltip=${tooltip::-2}

echo \{\"text\": \"$num_of_updates\", \"class\": \"$class\", \"tooltip\": \"$tooltip\"\}
