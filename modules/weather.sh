#!/bin/bash

source ~/.config/waybar/conf.local

if [[ $1 == "click" ]]; then
   firefox https://de.wttr.in/${WEATHER_LOCATION}
   exit
fi

cachedir=~/.cache/rbn
cachefile=${0##*/}-$1
# time
now=$(date +"%H")
# echo $now
night_time_start='21'
night_time_end='05'

if [ ! -d $cachedir ]; then
    mkdir -p $cachedir
fi

if [ ! -f $cachedir/$cachefile ]; then
    touch $cachedir/$cachefile
fi

# Save current IFS
SAVEIFS=$IFS
# Change IFS to new line.
IFS=$'\n'
# 1740
cacheage=$(($(date +%s) - $(stat -c '%Y' "$cachedir/$cachefile")))
if [ $cacheage -gt 10 ] || [ ! -s $cachedir/$cachefile ]; then
    data=($(curl -s https://en.wttr.in/${WEATHER_LOCATION}\?0qnT 2>&1))
    curl -s https://de.wttr.in/${WEATHER_LOCATION}\?T > ~/.config/waybar/modules/weather.tmp
    echo ${data[0]} | cut -f1 -d, > $cachedir/$cachefile
    echo ${data[1]} | sed -E 's/^.{15}//' >> $cachedir/$cachefile
    echo ${data[2]} | sed -E 's/^.{15}//' >> $cachedir/$cachefile
    echo ${data[3]} | sed -E 's/^.{15}//' >> $cachedir/$cachefile
fi

# add string which will lead to newlines in tooltip and replace problematic characters
tooltip=`sed -e '38,$d' -e 's/\"/¯/g' -e 's/[\]/╲/g' -e 's/$/\\\\\\\\n/' ~/.config/waybar/modules/weather.tmp`
# remove original newlines which would lead to leading spaces
tooltip=`tr -d '\n' <<< $tooltip`
# remove last newline
tooltip=${tooltip::-3}

# check if night
if [ $now -gt $night_time_start -o $now -lt $night_time_end ]; then
	night=1
else
	night=0
fi
# echo $night
weather=($(cat $cachedir/$cachefile))

# Restore IFSClear
IFS=$SAVEIFS

temperature=$(echo ${weather[2]})
# echo $temperature
# echo ${weather[1]##*,}
# night conditions   
# https://fontawesome.com/icons?s=solid&c=weather
case $(echo ${weather[1]##*,} | tr '[:upper:]' '[:lower:]') in
	"clear" | "sunny")
		if (($night)) ; then
			condition=""
		else
			condition=""
		fi
	;;
	"partly cloudy")
		if (($night)); then
			condition=""
		else
			condition=""
		fi
	;;
"cloudy")
    condition=""
    ;;
"overcast")
	if (($night)); then
		condition=""
	else
		condition=""
	fi
    ;;
"mist" | "fog" | "freezing fog")
    condition=""
    ;;
"patchy rain possible" | "patchy light drizzle" | "light drizzle" | "patchy light rain" | "light rain" | "light rain shower" | "rain")
    if (($night)); then
		condition=""
	else
		condition="" #  "
	fi
    ;;
"moderate rain at times" | "moderate rain" | "heavy rain at times" | "heavy rain" | "moderate or heavy rain shower" | "torrential rain shower" | "rain shower")
    condition=""
    ;;
"patchy snow possible" | "patchy sleet possible" | "patchy freezing drizzle possible" | "freezing drizzle" | "heavy freezing drizzle" | "light freezing rain" | "moderate or heavy freezing rain" | "light sleet" | "ice pellets" | "light sleet showers" | "moderate or heavy sleet showers")
    condition=""
    ;;
"blowing snow" | "moderate or heavy sleet" | "patchy light snow" | "light snow" | "light snow showers")
    condition=""
    ;;
"blizzard" | "patchy moderate snow" | "moderate snow" | "patchy heavy snow" | "heavy snow" | "moderate or heavy snow with thunder" | "moderate or heavy snow showers")
    condition="" #
    ;;
"thundery outbreaks possible" | "patchy light rain with thunder" | "moderate or heavy rain with thunder" | "patchy light snow with thunder")
    condition=""
    ;;
*)
    condition=""
    echo -e "{\"text\":\""$condition"\", \"alt\":\""${weather[0]}"\", \"tooltip\":\""${weather[0]}: $temperature ${weather[1]}"\"}"
    ;;
esac

# echo $temp $condition $night

echo -e "{\"text\":\""$temperature $condition"\", \"alt\":\""${weather[0]}"\", \"tooltip\":\"$tooltip\"}"
