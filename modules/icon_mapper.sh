#! /bin/bash

case $1 in
   firefox)
      icon=""
      ;;
   termite|XTerm)
      icon=""
      ;;
   thunderbird)
      icon=""
      ;;
   org.kde.okular)
      icon=""
      ;;
   Spotify)
      icon=""
      ;;
   discord)
      icon=""
      ;;
   *cloud*)
      icon=""
      ;;
   Steam)
      icon=""  # 
      ;;
   zoom)
      icon=""
      ;;
   Gimp)
      icon=""
      ;;
   minecraft-launcher|Minecraft)
      icon=""
      ;;
   librecad)
      icon=""
      ;;
   vlc)
      icon=""
      ;;
   Signal)
      icon=""
      ;;
   vampir)
      icon=""
      ;;
   null|'')
      icon=
      ;;
   *)
      icon=$1
      ;;
esac

echo $icon
